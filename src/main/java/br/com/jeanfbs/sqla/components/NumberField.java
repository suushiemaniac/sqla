package br.com.jeanfbs.sqla.components;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TextField;

public class NumberField extends TextField {

    private IntegerProperty maxLength = new SimpleIntegerProperty(this, "maxLength");

    @Override
    public void replaceText(int start, int end, String text) {
        String newText = null;
        if(text.length() <= maxLength.get()){
            newText = text;
        }else{

            newText = text.replaceAll("[^\\d]", ""). substring(0, maxLength.get());
        }

        super.replaceText(start, end, newText);
    }

    public Integer getMaxLength() {
        return maxLength.get();
    }

    public void setMaxLength(Integer value) {
        maxLength.setValue(value);
    }
}
