package br.com.jeanfbs.sqla.vo;

import br.com.jeanfbs.sqla.enums.QueryType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;


@Data
@RequiredArgsConstructor
public class Query<T> {

    private Optional<String> name = Optional.empty();
    @NonNull private String schema;
    @NonNull private String tableName;
    @NonNull private QueryType type;
    @NonNull private List<T> values;

    public String asString() {
        return String.format("INSERT INTO %s.%s (teste1, teste2) VALUES(valor1, valor2)", schema, tableName);
    }
}
