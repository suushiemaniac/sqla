package br.com.jeanfbs.sqla.vo;

import java.util.List;

public class QueryTabData {

    public Table table;
    public List<Query> queries;

}
