package br.com.jeanfbs.sqla.db.sqlserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
public class SQLConnectionAttributes {

    private String hostName;
    private String username;
    private String password;
    private Integer port;
    private String dataBase;


    @Override
    public String toString() {
        return String.format("jdbc:sqlserver://%s:%s;user=%s;password=***********;databaseName=%s",
                hostName,port,username,dataBase);
    }
}
