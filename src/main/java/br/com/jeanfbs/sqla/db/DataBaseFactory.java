package br.com.jeanfbs.sqla.db;

import br.com.jeanfbs.sqla.db.sqlserver.SQLConnectionAttributes;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import java.sql.Connection;

public interface DataBaseFactory {

    Connection getInstance(final DriverTypeEnum driverTypeEnum,
                           final SQLConnectionAttributes attrs) throws SQLServerException;

}
